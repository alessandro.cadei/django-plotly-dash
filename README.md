# django-plotly-dash

Simple example of a django app with the django-plotly-dash library.

## Getting started

Create a virtual environment:
```
python -m venv <venv_name>
```

Activate virtual environment (this vary basing on your operating system):
Linux:
```
source <venv_name>/bin/activate
```
Look at the references for more details.

Install requirements:
```
pip install -r requirements.txt
```

Move to the myapp outer directory.
```
cd myapp
```

Start the django server by running:
```
python manage.py runserver
```

Go to 'localhost:8000' to see the result.

## References

- [ ] [Virtual environments in python](https://docs.python.org/3/library/venv.html)
- [ ] [Django tutorial](https://docs.djangoproject.com/en/5.0/intro/tutorial01/)
- [ ] [Django plotly dash documentation](https://django-plotly-dash.readthedocs.io/en/latest/installation.html)
- [ ] [Django plotly dash video tutorial](https://www.youtube.com/watch?v=psvU4zwO3Ao)