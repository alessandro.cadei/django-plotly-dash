from django.urls import path
from . import views
from plotlyapp.dashapp.finishedapp import simplexample

urlpatterns = [
    path("", views.welcome, name="welcome"),
]